package Exercise8;

public class IllegalArgumentException extends Exception{

    public IllegalArgumentException(){

        super("You have to choose between Even and Odd!!");
    }
}
