package Exercise8;

interface IteratorIF {

       public int next();          //Returns the next element.
       public boolean hasnext();  //Checks whether the datastructur has further elements.

}
